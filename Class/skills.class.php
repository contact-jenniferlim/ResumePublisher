<?php class skills{

	private $id;
	private $skill;
	private $id_user;

	public function __construct(Array $array){
		if(isset($array['skill']))
        	    $this->setSkill($array['skill']);
		$this->setId_User($array['id_user']);
	}

	public function getId(){
		return $this->id;	
	}

	public function getSkill(){
		return $this->skill;
	}

	public function setSkill($skill){
		$this->skill=$skill;
	}

	public function getId_user(){
		return $this->id_user;
	}

	public function setId_user($id_user){
		$this->id_user=$id_user;
	}
}