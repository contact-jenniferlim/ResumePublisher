<?php

	class member{
		private $firstname;
		private $lastname;
		private $gender;
		private $phonenumber;
		private $email;
		private $password;
		private $dateOfInscription;

		public function __construct(Array $array)
		{
			if(isset($array['firstname']))
        	    $this->setFirstname($array['firstname']);
        	if(isset($array['lastname']))
    	        $this->setLastname($array['lastname']);
	        if(isset($array['gender']))
            	$this->setGender($array['gender']);
	        if(isset($array['phonenumber']))
            	$this->setPhoneNumber($array['phonenumber']);
			if(isset($array['emailSignUp']))
            	$this->setEmail($array['emailSignUp']);
            if(isset($array['passwordSignUp']))
            	$this->setPassword($array['passwordSignUp']);
            $this->setDateOfInscription();
		}

		public function getFirstname(){
			return $this->firstname;
		}

		public function setFirstname($firstname){
			$this->firstname = $firstname;
		}

		public function getLastname(){
			return $this->lastname;
		}

		public function setLastname($lastname){
			$this->lastname = $lastname;
		}

		public function getGender(){
			return $this->gender;
		}

		public function setGender($gender){
			$this->gender = $gender;
		}

		public function getPhoneNumber(){
			return $this->phonenumber;
		}

		public function setPhoneNumber($phonenumber){
			$this->phonenumber = $phonenumber;
		}

		public function getEmail(){
			return $this->email;
		}

		public function setEmail($email){
			$this->email = $email;
		}

		public function getPassword(){
			return $this->password;
		}

		public function setPassword($password){
			$this->password = sha1($password);
		}
		
		public function getDateOfInscription(){
			return $this->dateOfInscription;
		}

		public function setDateOfInscription()
		{
			$this->dateOfInscription = date("d/m/Y H:i:s");
		}
	}