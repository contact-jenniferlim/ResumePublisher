<?php class jobs{
	private $id;
	private $id_user;
	private $function;
	private $firm;
	private $whenFunction;
	private $whenEndFunction;
	private $descriptionFunction;

	public function __construct(Array $array){
		if(isset($array['function']))
        	    $this->setFunction($array['function']);
        if(isset($array['firm']))
        	    $this->setFirm($array['firm']);
        if(isset($array['whenFunction']))
        	    $this->setWhenFunction($array['whenFunction']);
        if(isset($array['whenEndFunction']))
        	    $this->setWhenEndFunction($array['whenEndFunction']);
        if(isset($array['descriptionFunction']))
        	    $this->setDescriptionFunction($array['descriptionFunction']);	         	
   		$this->setId_User($array['id_user']);
	}

	public function getId(){
		return $this->id;
	}

	public function getFunction(){
		return $this->function;
	}

	public function setFunction($function){
		$this->function = $function;
	}

	public function getFirm(){
		return $this->firm;
	}

	public function setFirm($firm){
		$this->firm = $firm;
	}

	public function getWhenFunction()
	{
		return $this->whenFunction;
	}

	public function setWhenFunction($date)
	{
		$this->whenFunction=$date;
	}

	public function getWhenEndFunction(){
		return $this->whenEndFunction;
	}

	public function setWhenEndFunction($date){
		$this->whenEndFunction = $date;
	}

	public function getDescriptionFunction(){
		return $this->descriptionFunction;
	}

	public function setDescriptionFunction($descriptionFunction){
		$this->descriptionFunction=$descriptionFunction;
	}

	public function getId_user (){
		return $this->id_user;
	}

	public function setId_user($id_user){
		$this->id_user = $id_user;
	}

}