<?php class studies{
	private $id;
	private $id_user;
	private $diploma;
	private $school;
	private $whenDiploma;

	public function __construct(Array $array){
		if(isset($array['diploma']))
        	    $this->setDiploma($array['diploma']);
        if(isset($array['school']))
        	    $this->setSchool($array['school']);
        if(isset($array['whenDiploma']))
        	    $this->setWhenDiploma($array['whenDiploma']);	         	
   		$this->setId_User($array['id_user']);
	}

	public function getId(){
		return $this->id;
	}

	public function getId_user(){
		return $this->id_user;
	}

	public function setId_User($id_user){
		$this->id_user=$id_user;
	}

	public function getDiploma(){
		return $this->diploma;
	}

	public function setDiploma($diploma){
		$this->diploma = $diploma;
	}

	public function getSchool(){
		return $this->school;
	}

	public function setSchool($school){
		$this->school = $school;
	}

	public function getWhenDiploma(){
		return $this->whenDiploma;
	}

	public function setWhenDiploma($date){
		$this->whenDiploma = $date;
	}
}