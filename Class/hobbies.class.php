<?php class hobbies{
	private $id;
	private $id_user;
	private $cathegory;
	private $hobby;

	public function __construct(Array $array){
		if(isset($array['cathegory']))
        	    $this->setCathegory($array['cathegory']);	  
        if(isset($array['hobby']))
        	    $this->setHobby($array['hobby']);	           	
   		$this->setId_User($array['id_user']);
	}

	public function getId(){
		return $this->id;
	}

	public function getId_user(){
		return $this->id_user;
	} 

	public function setId_user($id_user){
		$this->id_user = $id_user;
	}

	public function getCathegory(){
		return $this->cathegory;
	}

	public function setCathegory($cathegory){
		$this->cathegory = $cathegory;
	}

	public function getHobby(){
		return $this->hobby;
	}

	public function setHobby($hobby){
		$this->hobby = $hobby;
	}
}