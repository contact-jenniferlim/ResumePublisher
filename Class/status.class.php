<?php class status{

	private $id;
	private $status;
	private $_function;
	private $id_user;

	public function __construct(Array $array){
		if(isset($array['status']))
        	    $this->setStatus($array['status']);
		if(isset($array['_function']))
        	    $this->setFunction($array['_function']);
		$this->setId_User($array['id_user']);
	}

	public function getId(){
		return $this->id;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getFunction(){
		return $this->_function;
	}

	public function setFunction($function){
		$this->_function = $function;
	}

	public function getId_user(){
		return $this->id_user;
	}

	public function setId_user($id_user){
		$this->id_user = $id_user;
	}

}