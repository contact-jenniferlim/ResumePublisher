<div id="studies">
	<div class="row">
	  	<div class="col-md-12 cathegory">
	  		Studies
        <?php if ($edit==true) { ?>
	  		<button type="button" class="btn" data-toggle="modal" data-target="#ModalStudies">
        		<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
  			</button>
        <?php } ?>
	  	</div>
	</div>
  <?php if ($resultat6){ ?>
	<div class="row">
	  <div class="col-md-4">  <?php echo $resultat6['whenDiploma'] ?> </div>
	  <div class="col-md-4"> <?php echo $resultat6['diploma'] ?> </div>
    <div class="col-md-4"> <?php echo $resultat6['school'] ?> </div>      
	</div>
  <?php } ?>
	<br><br>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalStudies" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Studies</h4>
      </div>
      <div class="modal-body">
          <div id="formStudies">
            <form class="form-horizontal" method="post" action="../Controller/NewDiploma.php">
              <div class="form-group">
                <label for="diploma" class="col-sm-2 control-label">Diploma</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="diploma" placeholder="Doctor, Lawyer, Engineer">
                </div>
              </div>
              <div class="form-group">
                <label for="school" class="col-sm-2 control-label">In what school ?</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="school" placeholder="Cnam, Polytechnique, Centrale">
                </div>
              </div>
              <div class="form-group">
                <label for="whenDiploma" class="col-sm-2 control-label">When ?</label>
                <div class="col-sm-10">
                  <input type="date" name="whenDiploma" class="form-control" id="whenDiploma" >
                </div>
              </div>
              <input type="hidden" name="id_user" value="<?php echo $id ?>" >
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Save</button>
                </div>
              </div>
            </form>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>