<div id="jobs">
	<div class="row">
	  	<div class="col-md-12 cathegory">
	  		Jobs
	  		<?php if ($edit==true) { ?> 
	  		<button type="button" class="btn" data-toggle="modal" data-target="#ModalJobs">
    		    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
  			</button>
  			<?php } ?>
	  	</div>
	</div>
	<?php if ($resultat5) {  ?>
	<div class="row">
	  <div class="col-md-6"><?php echo $resultat5['function']; ?></div>
	  <div class="col-md-6"><?php echo $resultat5['firm']; ?></div>
	</div>
	<div class="row">
	  <div class="col-md-12">Missions</div>
	</div>
	<div class="row">
	  <div class="col-md-12"><?php echo $resultat5['descriptionFunction']; ?></div>
	</div>
	<?php  } ?>
	<br><br>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalJobs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Jobs</h4>
      </div>
      <div class="modal-body">
		    <div id="formJobs"> 
			  	<form class="form-horizontal" method="post" action="../Controller/Newjob.php">
				    <div class="form-group">
				      <label for="function" class="col-sm-2 control-label">Function</label>
				      <div class="col-sm-10">
				        <input type="text" class="form-control" name="function" placeholder="Doctor, Lawyer, Engineer">
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="firm" class="col-sm-2 control-label">In what firm ?</label>
				      <div class="col-sm-10">
				        <input type="text" class="form-control" name="firm" placeholder="Orange, Sfr, Louis Vuitton">
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="whenFunction" class="col-sm-2 control-label">When ?</label>
				      <div class="col-sm-10">
				        <input type="date" id="whenFunction" class="form-control" name="whenFunction" >
				      </div>
				    </div>
				  	<div class="form-group">
				      <label for="whenFunction" class="col-sm-2 control-label">When End Function?</label>
				      <div class="col-sm-10">
				        <input type="date" id="whenEndFunction" class="form-control" name="whenEndFunction" >
				      </div>
				    </div>
				    <div class="form-group">
				      <label for="descriptionFunction" class="col-sm-2 control-label">What did you do for this society?</label>
				      <div class="col-sm-10">
				        <input type="textarea" class="form-control" name="descriptionFunction" placeholder="*Take care of people's health *Take defense of people *Manage project">
				      </div>
				    </div>
				    <input type="hidden" name="id_user" value="<?php echo $id ?>" >
				    <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				        <button type="submit" class="btn btn-default">Save</button>
				      </div>
				    </div>
				 </form>
			</div>
      	</div>
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    </div>
    </div>
  </div>
</div>