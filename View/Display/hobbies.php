<div id="hobbies">
	<div class="row">
	  <div class="col-md-12 cathegory">Hobbies
        <?php if ($edit==true) { ?>
	    	<button type="button" class="btn" data-toggle="modal" data-target="#ModalHobbies">
      			<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
  			</button>
        <?php } ?>	
  		</div>
	</div>
  <?php if ($resultat7) { ?>
	<div class="row">
	  <div class="col-md-4"> <?php echo $resultat7['cathegory']; ?></div>
	  <div class="col-md-8"> <?php echo $resultat7['hobby']; ?> </div>  
	</div>
  <?php } ?>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalHobbies" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hobbies</h4>
      </div>
      <div class="modal-body">
      <div id="formHobbies">
        <form class="form-horizontal" method="post" action="../Controller/NewHobby.php">
          <div class="form-group">
            <label for="cathegory" class="col-sm-2 control-label">Cathegory</label>
            <div class="col-sm-10">
              <select class="form-control" name="cathegory">
                <option value="Culture">Culture</option>
                <option value="Sport">Sport</option>
                <option value="Charities">Charities</option>
                <option value="Travel">Travel</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="hobby" class="col-sm-2 control-label">Hobby</label>
            <div class="col-sm-10">
              <input type="textarea" class="form-control" name="hobby" placeholder="Sing, Dance, Cinema, Travel">
            </div>
          </div>
          <input type="hidden" name="id_user" value="<?php echo $id ?>" >
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default">Save</button>
            </div>
          </div>
        </form>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>