
<div id="userPresentation">
	<div class="row">
	  <div class="col-md-2">
	  	<?php if ($gender == "female") { ?>
	  		<img width="100px" height="100px" src="../Img/LogoFem.jpg">
	  	<?php }if( $gender == "male") { ?>
	  		<img width="100px" height="100px" src="../Img/LogoMale.jpg">
	  	<?php } ?>
	  </div>
	  <div class="col-md-6">
	  	<?php echo $firstname; echo ' '; echo $lastname; ?> <br>
	  	<?php echo $status; ?> 
	  	<?php if ($edit==true) { ?> 
	  	<button type="button" class="btn" data-toggle="modal" data-target="#ModalStatus">
  			<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
		</button> <br>
	  	<?php } ?>
	  	<?php echo $function; ?> <br>
	  	<?php echo $email; ?><br>
	  	<?php echo $phoneNumber; ?><br> 
	  </div>
	</div>
	<div class="row">
		<hr>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Status</h4>
      </div>
      <div class="modal-body">
       <div id="formStatus">
		  <form class="form-horizontal" method="post" action="../Controller/status.php">
		    <div class="form-group">
		      <label for="_function" class="col-sm-2 control-label">Function</label>
		      <div class="col-sm-10">
		        <input type="text" class="form-control" name="_function" placeholder="Doctor, Lawyer, Engineer">
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="status" class="col-sm-2 control-label">Who are you?</label>
		      <div class="col-sm-10">
		        <input type="text" class="form-control" name="status" placeholder="Dynamic woman who search a job">
		      </div>
		    </div>
		    <input type="hidden" name="id_user" value="<?php echo $id ?>" >
		    <div class="form-group">
		      <div class="col-sm-offset-2 col-sm-10">
		        <button type="submit" class="btn btn-default">Save</button>
		      </div>
		    </div>
		  </form>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>