<form class="form-inline" method="post" action="Controller/Login.php">
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" name="email" placeholder="contact.jenniferlim@gmail.com">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" name="password" >
  </div>
  <button type="submit" class="btn btn-default">Sign In</button>
</form>
  
  <h5> Or Sign up? </h5>

<form class="form-horizontal" method="post" action="Controller/NewUser.php">
  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label">First Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="firstname" placeholder="Jennifer">
    </div>
  </div>
  <div class="form-group">
    <label for="lastname" class="col-sm-2 control-label">Lastname</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="lastname" placeholder="Lim">
    </div>
  </div>
  <div class="form-group">
    <label for="gender" class="col-sm-2 control-label">Gender</label>
    <div class="col-sm-1">
      <label>
        <input type="radio" name="gender" id="female" value="female">
        F  
      </label>
    </div>
    <div class="col-sm-1">
      <label>
        <input type="radio" name="gender" id="male" value="male">
        M  
      </label>
    </div>    
  </div>
  <div class="form-group">
    <label for="phonenumber" class="col-sm-2 control-label">Phone Number</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="phonenumber" placeholder="+33788335060">
    </div>
  </div>     
  <div class="form-group">
    <label for="emailSignUp" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" name="emailSignUp" placeholder="contact.jenniferlim@gmail.com">
    </div>
  </div>
  <div class="form-group">
    <label for="passwordSignUp" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="passwordSignUp" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Sign up</button>
    </div>
  </div>
</form>