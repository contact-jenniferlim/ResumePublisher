-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 27 Mai 2015 à 14:57
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `resumepublisher`
--

-- --------------------------------------------------------

--
-- Structure de la table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `cathegory` varchar(255) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `hobbies`
--

INSERT INTO `hobbies` (`id`, `id_user`, `cathegory`, `hobby`) VALUES
(1, 1, 'Culture', 'Sing, Piano');

-- --------------------------------------------------------

--
-- Structure de la table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `function` varchar(255) NOT NULL,
  `firm` varchar(255) NOT NULL,
  `whenFunction` varchar(255) NOT NULL,
  `whenEndFunction` varchar(255) NOT NULL,
  `descriptionFunction` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `jobs`
--

INSERT INTO `jobs` (`id`, `id_user`, `function`, `firm`, `whenFunction`, `whenEndFunction`, `descriptionFunction`) VALUES
(1, 1, 'Engineer', 'Louis Vuitton', '0000-00-00', '0000-00-00', '* Take care of software');

-- --------------------------------------------------------

--
-- Structure de la table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Firstname` varchar(255) NOT NULL,
  `Lastname` varchar(255) NOT NULL,
  `Gender` varchar(255) NOT NULL,
  `Phonenumber` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `DateOfInscription` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `members`
--

INSERT INTO `members` (`id`, `Firstname`, `Lastname`, `Gender`, `Phonenumber`, `Email`, `Password`, `DateOfInscription`) VALUES
(1, 'Jennifer', 'Lim', 'female', '+33788335060', 'contact.jenniferlim@gmail.com', '0e32b70509410339680139e268ea275aa7bcb6f9', '2015-04-30'),
(2, 'Test', 'Test', 'male', '+33788335060', 'test@test.com', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '2015-04-30'),
(3, 'Gael', 'Bitane', 'male', '+33685368149', 'gael.bitane@gmail.com', 'f9974659649e3b034a63784d8f35df42716fbd6d', '2015-05-06'),
(4, 'Test2', 'Test2', 'female', '+33788335060', 'test@test.com', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '2015-05-06');

-- --------------------------------------------------------

--
-- Structure de la table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `skills`
--

INSERT INTO `skills` (`id`, `skill`, `id_user`) VALUES
(1, 'Manage projects', 1),
(2, 'Develop software', 1);

-- --------------------------------------------------------

--
-- Structure de la table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  `_function` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `status`
--

INSERT INTO `status` (`id`, `status`, `_function`, `id_user`) VALUES
(1, 'Dynamic woman who search a job', 'Engineer', 1);

-- --------------------------------------------------------

--
-- Structure de la table `studies`
--

CREATE TABLE IF NOT EXISTS `studies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `diploma` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL,
  `whenDiploma` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `studies`
--

INSERT INTO `studies` (`id`, `id_user`, `diploma`, `school`, `whenDiploma`) VALUES
(1, 1, 'Engineer in software development', 'Cnam', '05/01/2015');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
