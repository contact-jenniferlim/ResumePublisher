<?php require_once("View/head.php"); ?>
<body>
	<div class="container">
		<H3> Welcome to the Resume Publisher! </H3>
		<?php require_once("View/Forms/user.php"); ?>
		<?php require_once("View/Display/listofresume.php")	?>	
		<?php require_once("View/Forms/choices.php"); ?>		
		<?php require_once("View/Forms/status.php"); ?>
		<?php require_once("View/Forms/skills.php"); ?>
		<?php require_once("View/Forms/jobs.php"); ?>
		<?php require_once("View/Forms/studies.php"); ?>
		<?php require_once("View/Forms/hobbies.php"); ?>
		<!--My Resume -->
		<div id="Resume">
			<?php require_once("View/Display/user.php"); ?>
			<?php require_once("View/Display/skills.php"); ?>
			<?php require_once("View/Display/jobs.php"); ?>
			<?php require_once("View/Display/studies.php"); ?>
			<?php require_once("View/Display/hobbies.php"); ?>
		</div>
	</div>
	
</body>
<?php require_once("View/footer.php"); ?>