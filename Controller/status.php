<?php require_once('../Class/status.class.php');

//Connexion à la bdd

 try {
        $bdd = new PDO('mysql:host=localhost;dbname=resumepublisher;charset=utf8', 'root', '');
    }
    catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
    }

$newStatus = new status($_POST);

$status = $newStatus->getStatus();
$function = $newStatus->getFunction();
$id_user = $newStatus->getId_user();

// Vérification status déjà present

$req = $bdd->prepare('SELECT * FROM status WHERE id_user = :id_user');

$req->execute(array(

    'id_user' => $id_user));


$resultat = $req->fetch();

if(!$resultat){
	$req2 = $bdd->prepare('INSERT INTO status(status, _function, id_user) VALUES(:status, :function, :id_user)');

	$req2->execute(array(

	    'status' => $status,

	    'function' => $function,

	    'id_user' => $id_user));

	$resultat2 = $req2->fetch();

	echo "Your status has been filled."; ?>

	<a href="../Index.php" > Retour </a>

<?php }else{
	$req2 = $bdd->prepare('UPDATE status SET status = :status, _function = :function WHERE (id_user == :id_user)');

	$req2->execute(array(

	    'status' => $status,
	    'function' => $function,
	    'id_user' => $id_user));

	$resultat2 = $req2->fetch();

	echo "Your status has been updated."; ?>

	<a href="../Index.php" > Retour </a>
<?php } ?>