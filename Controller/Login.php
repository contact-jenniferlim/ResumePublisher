<?php

require_once("../View/head.php");

//Connexion à la bdd

 try {
        $bdd = new PDO('mysql:host=localhost;dbname=resumepublisher;charset=utf8', 'root', '');
    }
    catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
    }

$email = $_POST['email'];
$pass_hache = sha1($_POST['password']);

// Vérification des identifiants

$req = $bdd->prepare('SELECT id FROM members WHERE email = :email AND password = :password');

$req->execute(array(

    'email' => $email,

    'password' => $pass_hache));


$resultat = $req->fetch();

if (!$resultat)

{

    echo 'Mauvais identifiant ou mot de passe !';

 ?>

 <a href="../index.php"> Retour a l'accueil </a>
<?php
}

else

{
	$req2 = $bdd->prepare('SELECT * FROM members WHERE email = :email AND password = :password');
	$req2->execute(array(

    'email' => $email,

    'password' => $pass_hache));

	$resultat2 = $req2->fetch();
	$id = $resultat2['id'];
	$firstname = $resultat2['Firstname'];
	$lastname = $resultat2['Lastname'];
	$gender = $resultat2['Gender'];
	$phoneNumber = $resultat2['Phonenumber'];

    session_start();

    $_SESSION['id'] = $resultat['id'];

    $_SESSION['email'] = $email;

    $req3 = $bdd->prepare('SELECT * FROM status WHERE id_user = :id_user');
    $req3->execute(array(

    'id_user' => $id));

    $resultat3=$req3->fetch();
    $status=$resultat3['status'];
    $function = $resultat3['_function'];

    $req4 = $bdd->prepare('SELECT skill FROM skills WHERE id_user = :id_user');
    $req4->execute(array(

    'id_user' => $id));
    $resultat4=$req4->fetch();

    $req5 = $bdd->prepare('SELECT * FROM jobs WHERE id_user = :id_user');
    $req5->execute(array(
    'id_user' => $id));
    $resultat5=$req5->fetch();

    $req6 = $bdd->prepare('SELECT * FROM studies WHERE id_user = :id_user');
    $req6->execute(array(
    'id_user' => $id));
    $resultat6=$req6->fetch();

    $req7 = $bdd->prepare('SELECT * FROM hobbies WHERE id_user = :id_user');
    $req7->execute(array(
    'id_user' => $id));
    $resultat7=$req7->fetch();

    $edit=true;
    require_once('../View/Display/connected.php');

}  