<?php require_once('../Class/studies.class.php');

//Connexion à la bdd

 try {
        $bdd = new PDO('mysql:host=localhost;dbname=resumepublisher;charset=utf8', 'root', '');
    }
    catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
    }

$newDiploma = new studies($_POST);


$diploma = $newDiploma->getDiploma();
$school = $newDiploma->getSchool(); 
$whenDiploma = $newDiploma->getWhenDiploma();
$id_user = $newDiploma->getId_user(); 

	$req = $bdd->prepare('INSERT INTO studies(diploma, school, whenDiploma, id_user) VALUES(:diploma, :school, :whenDiploma, :id_user)');

	$req->execute(array(

	    'diploma' => $diploma,

	    'school' => $school,

	    'whenDiploma' => $whenDiploma,

	    'id_user' => $id_user));

	$resultat = $req->fetch();

	echo "Your diploma has been added."; ?>

	<a href="../Index.php" > Retour </a>