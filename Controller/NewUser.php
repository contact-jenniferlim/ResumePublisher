<?php

require_once("../View/head.php");
require_once("../Class/member.class.php");
//Connexion à la bdd

 try {
        $bdd = new PDO('mysql:host=localhost;dbname=resumepublisher;charset=utf8', 'root', '');
    }
    catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
    }

// Vérification de la validité des informations

$newMember= new member($_POST);

$firstname = $newMember->getFirstname();
$lastname = $newMember->getLastname();
$gender = $newMember->getGender();
$phonenumber = $newMember->getPhoneNumber();
$email = $newMember->getEmail();
$password = $newMember->getPassword();

// Insertion

$req = $bdd->prepare('INSERT INTO members(Firstname, Lastname, Gender, Phonenumber, Email, Password, DateOfInscription) VALUES(:firstname, :lastname, :gender, :phonenumber, :email, :password, CURDATE())');

$req->execute(array(

    'firstname' => $firstname,

    'lastname' => $lastname,

    'gender' => $gender,

    'phonenumber' => $phonenumber,

    'email' => $email,

    'password' => $password));

echo 'Votre compte a ete cree. Merci de vous connecter pour acceder a la liste des CV ou a votre CV.';
?>
<br>
<a href="../index.php"> Retour a l'accueil </a>

<?php require_once("../View/footer.php"); 