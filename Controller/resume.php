<?php  

//Connexion à la bdd

 try {
        $bdd = new PDO('mysql:host=localhost;dbname=resumepublisher;charset=utf8', 'root', '');
    }
    catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
    }

    $id = $_POST['id'];

	$req = $bdd->prepare('SELECT * FROM members WHERE id = :id');
	$req->execute(array(

    'id' => $id));
    $resultat= $req->fetch();
	$firstname = $resultat['Firstname'];
	$lastname = $resultat['Lastname'];
	$gender = $resultat['Gender'];
	$phoneNumber = $resultat['Phonenumber'];

    $req2 = $bdd->prepare('SELECT * FROM status WHERE id_user = :id_user');
    $req2->execute(array('id_user' => $id));
    $resultat2=$req2->fetch();
    $status=$resultat2['status'];
    $function = $resultat2['_function'];

	$req4 = $bdd->prepare('SELECT skill FROM skills WHERE id_user = :id_user');
    $req4->execute(array(

    'id_user' => $id));
    $resultat4=$req4->fetch();

    $req5 = $bdd->prepare('SELECT * FROM jobs WHERE id_user = :id_user');
    $req5->execute(array(
    'id_user' => $id));
    $resultat5=$req5->fetch();

    $req6 = $bdd->prepare('SELECT * FROM studies WHERE id_user = :id_user');
    $req6->execute(array(
    'id_user' => $id));
    $resultat6=$req6->fetch();

    $req7 = $bdd->prepare('SELECT * FROM hobbies WHERE id_user = :id_user');
    $req7->execute(array(
    'id_user' => $id));

    $edit=false;	
    ?>
<div id="Resume">
	<h3> Resume </h3> <br>

	<?php require_once("View/user.php"); ?>
	<?php require_once("View/skills.php"); ?>
	<?php require_once("View/jobs.php"); ?>
	<?php require_once("View/studies.php"); ?>
	<?php require_once("View/shobbies.php"); ?>
</div>
<?php require_once("View/footer.php"); ?>